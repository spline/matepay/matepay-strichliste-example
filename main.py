from flask import Flask, render_template, request, redirect
import requests
import os

app = Flask(__name__)

MATEPAY_URL = "https://pay.spline.de"
OWN_URL = "http://localhost"


@app.route("/")
def index():
    items = requests.get(f"{MATEPAY_URL}/api/v1/items/fsikasse").json()
    return render_template("index.html", items=items)


@app.route("/thx")
def thanks():
    return "thx"


@app.route("/pay", methods=("POST",))
def pay():
    # Parse the chosen items from the form,
    # to create a list of integer product ids.
    items = []
    for key, amount in request.form.items():
        item_id = int(key.replace("item-", ""))
        amount = int(amount)
        for _ in range(amount):
            items.append(item_id)

    # Avoid sending invalid (empty) requests to matepay
    if items == []:
        return redirect("/")

    # Create the payment request
    response = requests.post(
        f"{MATEPAY_URL}/api/v1/app_server/add_payment_request",
        headers={"MatePay-App-Token": os.environ["APP_TOKEN"]},
        json={"product_ids": items, "redirect_url": f"{OWN_URL}/thx"},
    ).json()

    # Redirect the user to the confirmation page
    return redirect(response["payment_url"])
