# MatePay App Server Example Project

This project provides a starting point for integrating your own application with MatePay.

## Setup

1. Go to [pay.spline.de](https://pay.spline.de)
2. log in with the account that you want to use for your application.
3. Open the developer tools, and reload the page.
4. Pick the request to `home`, and copy the value of the `MATEPAY-TOKEN` cookie. You can find it under the cookies tab.
5. Use the retrieved token to swap it for a longer-lived one: `curl -X POST -H "MATEPAY-TOKEN: <token>" https://pay.spline.de/api/v1/app_server/register`
6. Create a new file `.env` in this repository, and add `APP_TOKEN=<the token from the response>`

## Starting

Then, you can run the example application in flask.
```bash
flask --app main run
```

## Developing your own

In [main.py](main.py) you can find a minimal but working example for the matepay app server API.
The route for `/` shows how to retrieve the available items from your account.
The route for `/pay` shows how to create a payment request.

For more advanced usage, have a look at [MatePrint](https://gitlab.spline.inf.fu-berlin.de/spline/matepay/mateprint)
